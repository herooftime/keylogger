#include "Timer.hpp"
#include "mingw-std-threads-master/mingw.thread.h"

#include <chrono>

Timer::Timer()
{
  // Intentionally blank
}

Timer::Timer(const std::function<void(void)> &funct)
  : function(funct)
{
  // Intentionally blank
}

Timer::Timer(const std::function<void(void)> &funct, const unsigned long int &integer, const long int repeat)
  : function(funct), function_interval(std::chrono::milliseconds(integer)), call_number(repeat)
{
  // Intentionally blank
}

// Mutators
void Timer::setInterval(const unsigned long int &integer)
{
  if (alive)
    return; // prevents setting interval while in use

  function_interval = std::chrono::milliseconds(integer);
}

void Timer::setRepeatCount(const long int repeat)
{
  if (alive)
    return; // prevents setting interval while in use

  call_number = repeat;
}

void Timer::setFunction(const std::function<void(void)> &funct)
{
  function = funct;
}

// Accessors
bool Timer::getAlive() const
{
  return alive;
}

unsigned long int Timer::getInterval() const
{
  return function_interval.count(); // int instead of std::chrono::milliseconds
}

long int Timer::getRemainingCount() const
{
  return repeat_count;
}

long int Timer::getRepeatCount() const
{
  return call_number;
}

const std::function<void(void)> Timer::getFunction() const
{
  return function;
}

// General Functions
void Timer::startThread(bool aSync)
{
  if (getAlive())
    return; // prevents new thread while running

  alive = true;

  repeat_count = call_number;

  if (aSync)
    Thread = std::thread(threadFunction, this); // (function to be executed, where)

  else
    threadFunction();
}

void Timer::stopThread()
{
  alive = false;
  Thread.join();
}

// Private Functions

void Timer::sleepAndRun()
{
  std::this_thread::sleep_for(function_interval);

  if (alive)
    getFunction()();
}

void Timer::threadFunction()
{
  if (call_number == PERPETUAL)
    while (alive)
      sleepAndRun();

  else
    while (repeat_count--)
      sleepAndRun();
}
