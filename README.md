# keylogger

This repository contains a keylogger that was created with the help of a
tutorial, although it has many edits and personal changes (with more to come).

## Contact

mr.carpinelli@protonmail.ch

## Contributing

This is mostly a personal project, but any contributions are welcome and
appreciated.

## Building

No extra help will be provided in building this project from source. I do not
want to be the one responsible for misuse of this code, so hopefully this
provides a bit of a barrier. The hope is people who know enough to write the
code and compile it themselves as well as people who care to put in enough
effort to figure it (it's not hard if you just read the code) will be more
responsible with the code.

## License

All contributions are made under the [GNU General Public License v3](https://www.gnu.org/licenses/gpl-3.0.en.html). See the [LICENSE](LICENSE).
