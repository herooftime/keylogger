#ifndef BASE64_HPP
#define BASE64_HPP

#include <string>

namespace Base64
{
  std::string base64Encode(const std::string &str);
  // Returns a string encoded in Base64

  std::string encryptBase64(std::string str);
  // Returns an string encrypted using a Base64 and salt method
}

#endif // BASE64_HPP
