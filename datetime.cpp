#include "DateTime.hpp"

#include <ctime>
#include <fstream>

DateTime::DateTime()
{
  time_t epoch; // time in seconds from epoch
  time(&epoch); // takes system time and passes it to the time_t variable

  struct tm *time_info = localtime(&epoch); // put time info comprehensively into struct

  day = time_info->tm_mday;
  month = time_info->tm_mon + 1; // begin index at 1
  year = time_info->tm_year + 1900; // get year, MS time is from year 1900

  seconds = time_info->tm_sec;
  minutes = time_info->tm_min;
  hour = time_info->tm_hour;
}

DateTime::DateTime(int day, int month, int year, int hour, int minutes, int seconds)
  : day(day), month(month), year(year),
    hour(hour), minutes(minutes), seconds(seconds)
{
  // Intentionally blank
}

DateTime::DateTime(int day, int month, int year)
  : day(day), month(month), year(year),
    hour(0), minutes(0), seconds(0)
{
  // Intentionally blank
}

DateTime DateTime::getTimeStruct() const
{
  return DateTime();
}

std::string DateTime::getDate() const
{
  std::string date;

  date = std::string(day < 10 ? "0" : "") + std::to_string(day); // DD
  date += std::string(month < 10 ? ".0" : ".") + std::to_string(month); // .mm
  date += "." + std::to_string(year); // .yyyy

  return date;
}

std::string DateTime::getTime(const std::string &separator) const
{
  std::string time;

  time = std::string(hour < 10 ? "0" : "") + std::to_string(hour); // HH
  time += separator + std::string(minutes < 10 ? "0" : "") + std::to_string(minutes); // :MM
  time += separator + std::string(seconds < 10 ? "0" : "") + std::to_string(seconds); // :SS

  return time;
}

std::string DateTime::getDateTime(const std::string &separator) const
{
  return getDate() + " " + getTime(separator); // DD:mm:yyyy HH:MM:SS
}
