#include "Base64.hpp"

#include <string>

namespace Base64
{
  std::string base64Encode(const std::string &str)
  {
    const std::string &BASE64_TABLE = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"; // pass string itself with &, const keeps from changing
    const unsigned int BASE_63 = 0x3F; // 63 in hexadecimal, 111111

    std::string encoded;
    int value = 0; // start at 0 for binary shifting
    int bits = -6; // grouping by 6 bits (2^6 = 64)

    for (const char &character : str) // foreach, CONSIDER char instead of auto
      {
        value = (value << 8) + character; // left-binary shifting to convert to 1 byte size, Base64 encodes using 3 bytes at a time
        bits += 8; // 1 more byte/character processes

        while (bits >= 0) // while there are bits to encode
          {
            // -6 bits + 8 = 2 bits, right-binary shifting by 2 bits (from 8) to get 6 bits: first character
            // 2 bits leftover, - 6 bits = -4 (exit while < 0)
            // -4 + 8 from above block to get to 4 bits
            // shift over 4 bits from 8 to get 4 bits with 2 leftover bits to make 6 bits: second character
            // with 4 leftover bits 4 - 6 = -2 (exit while < 0)
            // -2 + 8 = 6 bits
            // shift over 6 bits from 8 to get 2 bits with 4 leftover bits to make 6 bits: third character
            // - 6 = 0 which re-loops while to add leftover 6 bits, resets to -6 for even 3 bytes (+ 8 three times)

            // CONSIDER removing logical AND, test if works, CONSIDER potentially to ensure 6 bits written
            // logical AND operation (&), checks corresponding binary place digits
            // e.g. 101 & 011 both have 1 in the first digit so = 001
            // push_back appends character
            encoded.push_back(BASE64_TABLE[(value >> bits) & BASE_63]); // pass corresponding 6-bit number as index for BASE64_TABLE, append to encodement

            bits -= 6; // 6 bits processed to character
          }
      }

    // if (bits == -4) then there are 2 bits to process, (value << 8) shifts it to 10 bits (2 bits + 8 trailing '0' bits)
    // >> (bits + 8) shifts it over to be 6 bits with 4 '0's trailing, (later added with 2 empty bytes to make 3 '=' (padding)

    // if (bits == -2) then there are 4 bits to process, (value << 8) shifts it to 12 bits (4 bits + 8 trailing '0' bits)
    /// >> (bits + 8) shifts it over to be 12 bits (/ 6 = 2 encoded characters) with no '0's trailing, (later added with 1 empty byte to make 1 '=' (padding)

    // bits could either be -4 or -2, -6 does not trigger, 0 leads to -6
    if (bits > -6) // if bytes processes not divisible by 3 (bytes)
      encoded.push_back(BASE64_TABLE[((value << 8) >> (bits + 8)) & BASE_63]); // maybe to ensure a size of 6 bits unsure, research

    // 4 characters in Base64 = 3 bytes, if the remainder of the size divided by 4 is not even,
    // then the data size was not divisible by 3 bytes (encodement not divible by 4), requiring up to 3 '=' paddings
    // up to two extra bytes of padding '0' bits (some used for remainder bytes) are converted to the '='
    // converted to 1 or 3 '=' depending on if there are 2 bytes in the remainder, or 1 byte, respectively

    // down to 8 bits (to be encoded) over divisible by 3, / 6 = 1 (encoded character) + extra bits (= 2),
    // this is added with another 2 bytes (to make divisible by 3) of '0's, 18 bits total, / 6, represented as 3 '='

    // up to 16 bits (to be encoded) over divisible by 3, / 6 = 2 (encoded character) + extra bits (= 4),
    // this is added with another byte (to make it divisible by 3) of '0's, 12 bits total, / 6, represented as 2 '='

    while (encoded.size() % 4) // while there is a remainder
      encoded.push_back('='); // append '=' padding (due 3-byte grouping)

    return encoded;
  }

  std::string encryptBase64(std::string str)
  {
    // Salts
    const std::string SALT[13] = {"c3G/&n", ",Fs@hLO", "Abv", "T*xqS", "tImU*M4P", "|&v7|", "k",
                                  "%CnXRix", "?jS", "Vak$6", "MOf@u=v=", "&w%w4'_L3|,w:EzS", "3u=:"};

    str = SALT[10] + str + SALT[11] + SALT[12];

    str = base64Encode(str);
    str.insert(7, SALT[2]);
    str += SALT[10];

    str = base64Encode(str);
    str += SALT[9] + SALT[12] + SALT[11];

    str = base64Encode(str);
    str.insert(1, "K");
    str.insert(7, "H");

    str = SALT[8] + SALT[1] + str + SALT[5];

    str.insert(3, SALT[4]);
    str.insert(2, SALT[3]);
    str.insert(8, SALT[3]);
    str.insert(6, SALT[0]);
    str.insert(15, SALT[6]);
    str.insert(12, SALT[2]);

    str = str + str + str + SALT[4];
    str = base64Encode(str);

    return str;
  }
}
