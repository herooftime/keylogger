#ifndef IO_HPP
#define IO_HPP

#include "Base64.hpp"
#include "DateTime.hpp"

#include <fstream>
#include <sstream>

namespace IO
{
  std::string getPath(const bool append_separator = false);
  // Returns current path, parameter appends a '\' separator to the returned path

  bool make_director(std::string path);
  // Returns true if successful or if directory already exists

  bool make_path(std::string path);
  // Returns true if successful or if path already exists
  // makes a new directory at each '\' in path to make full path

  void writeDeveloperLog(const std::string &str);
  // Writes to str to developer log

  template <class T>
  std::string writeLog(const T &variable);
  // Returns name of log if successful and returns an empty string if failed

  template <class T>
  std::string writeLog(const T &variable)
  {
    std::string path;
    std::string name;
    DateTime dateTime;
    std::ofstream logFile;

    path = getPath(true);
    name = dateTime.getDateTime("_");

    try
    {
      std::ostringstream sstring;
      std::string data;

      logFile.open(path + name);
      if (logFile.fail())
        return "";

      sstring << "[" << dateTime.getDateTime() << "]\n"
              << variable << std::endl;

      data = Base64::encryptBase64(sstring.str());

      logFile << data;
      if (logFile.fail())
        return "";

      logFile.close();

      return name;
    }

    catch(...)
    {
      return "";
    }
  }
}

#endif // IO_HPP
