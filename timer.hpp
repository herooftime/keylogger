#ifndef TIMER_HPP
#define TIMER_HPP

#include "mingw-std-threads-master/mingw.thread.h"
#include <chrono>

class Timer
{
public:
  Timer();

  Timer(const std::function<void(void)> &funct);

  Timer(const std::function<void(void)> &funct, const unsigned long int &integer, const long int repeat = Timer::PERPETUAL);

  static const long int PERPETUAL = -1L;

  // Mutators

  void setInterval(const unsigned long &integer);

  void setRepeatCount(const long int repeat);

  void setFunction(const std::function<void(void)> &funct);

  // Accessors

  bool getAlive() const;

  unsigned long int getInterval() const;

  long int getRemainingCount() const;

  long int getRepeatCount() const;

  const std::function<void(void)> getFunction() const;

  // Thread Functions

  void startThread(bool aSync = true);

  void stopThread();

private:
  std::function<void(void)> function = nullptr;
  std::chrono::milliseconds function_interval = std::chrono::milliseconds(0);
  std::thread Thread;

  bool alive = false;
  long int call_number = PERPETUAL;
  long int repeat_count = PERPETUAL;

  void sleepAndRun();

  void threadFunction();
};

#endif // TIMER_HPP
