#ifndef DATETIME_HPP
#define DATETIME_HPP

#include <string>

struct DateTime
{
  DateTime();

  DateTime(int day, int month, int year, int hour, int minutes, int seconds);

  DateTime(int day, int month, int year);

  int day;
  int month;
  int year;
  int hour;
  int minutes;
  int seconds;

  DateTime getTimeStruct() const;

  std::string getDate() const;

  std::string getTime(const std::string &separator = ":") const;

  std::string getDateTime(const std::string &separator = ":") const;
};

#endif // DATETIME_HPP
